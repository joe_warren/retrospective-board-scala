package controllers

import javax.inject._
import play.api._
import play.api.mvc._
import net.glxn.qrgen.javase.QRCode
import net.glxn.qrgen.core.image.ImageType
import model.RetrospectiveStore

@Singleton
class QRController @Inject()(cc: ControllerComponents) extends AbstractController(cc) {
  def qrCode(url: String) = Action { implicit request: Request[AnyContent] =>
    val binaryImage = QRCode.from(url).to(ImageType.PNG).stream()
    Ok(binaryImage.toByteArray).as("image/png")
  }
}
