package controllers

import javax.inject._
import play.api._
import play.api.mvc._
import play.twirl.api.Html

import model.RetrospectiveStore

@Singleton
class RetrospectiveController @Inject()(rs: RetrospectiveStore, cc: ControllerComponents) extends AbstractController(cc) with i18n.I18nSupport {

  def index() = Action { implicit request: Request[AnyContent] =>
    Ok(views.html.index())
  }
  def newRetrospective() = Action { implicit request: Request[AnyContent] => {
      val id = rs.createRetrospective()
      Redirect(routes.RetrospectiveController.disambiguate(id))
    }
  }
  def disambiguate(id: String) = Action { implicit request: Request[AnyContent] =>
    Ok(views.html.disambiguate()(id))
  }

  def display(id: String) = Action { implicit request: Request[AnyContent] =>
    rs.getRetrospective(id) match {
      case None => Status(400)
      case Some(retro) => Ok(views.html.display(id, retro))
    }
  }

  val commentForm = data.Form(
    data.Forms.mapping(
      "content" -> data.Forms.text,
      "type" -> data.Forms.text
      )(model.Comment.fromStrings)
      ({x:model.Comment => x.toStrings} andThen Some.apply)
  )
  def participate(id: String) = Action { implicit request: Request[AnyContent] =>
    Ok(views.html.participate(id, commentForm))
  }
  def newComment(id: String) = Action { implicit request: Request[AnyContent] =>{
      commentForm.bindFromRequest.fold(
        formWithErrors => BadRequest(views.html.participate(id, formWithErrors)),
        comment => {
          rs.addComment(id, comment)
          Redirect(routes.RetrospectiveController.participate(id))
        }
      )
    }
  }
}
