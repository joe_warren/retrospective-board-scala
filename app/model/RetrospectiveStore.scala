package model
import com.google.inject.ImplementedBy

@ImplementedBy(classOf[TempRetrospectiveStore])
trait RetrospectiveStore {
  def getRetrospective(id: String): Option[Retrospective]
  def addComment(retroID: String, comment: Comment): Unit
  def createRetrospective(): String
  def newRetrospective(): (String, Retrospective) = {
    val id = createRetrospective
    (id, getRetrospective(id).get)
  }
}
