package model
import java.util.UUID
import javax.inject.Singleton

@Singleton
class TempRetrospectiveStore extends RetrospectiveStore {
  // Yeah, I know this is mutable state, but we're modeling a DB here,
  // and I can't be bothered to write this with actions
  var retrospectives = Map[String, Retrospective]()

  def createRetrospective(): String = {
    val key = UUID.randomUUID().toString()
    retrospectives = retrospectives + (key -> Retrospective(List.empty))
    key
  }
  def getRetrospective(id: String): Option[Retrospective] = {
    retrospectives.get(id)
  }
  def addComment(retroID: String, comment: Comment): Unit = {
    val oldRetro = retrospectives(retroID)
    val newRetro = oldRetro.copy(comments = comment +: oldRetro.comments)
    retrospectives = retrospectives + (retroID -> newRetro)
  }
}
