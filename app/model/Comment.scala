package model

import java.text.ParseException

sealed trait CommentType{
}

object CommentTypeNames {
  val good = "Good"
  val bad = "Bad"
  val idea = "Idea"
  val props = "Props"
  val all = List(good, bad, idea, props)
}

case class GoodComment() extends CommentType {
  override def toString() = CommentTypeNames.good
}
case class BadComment() extends CommentType {
  override def toString() = CommentTypeNames.bad
}
case class IdeaComment() extends CommentType {
  override def toString() = CommentTypeNames.idea
}
case class PropsComment() extends CommentType {
  override def toString() = CommentTypeNames.props
}

case class Comment(content: String, typeOf: CommentType){
  def toStrings(): (String, String) = (content, typeOf.toString())
}

object Comment{
  def fromStrings(content: String, typeString: String) = {
    val ofType = typeString match {
      case CommentTypeNames.good => GoodComment()
      case CommentTypeNames.bad => BadComment()
      case CommentTypeNames.idea => IdeaComment()
      case CommentTypeNames.props => PropsComment()
      case _ => throw new ParseException(s"Bad Type String $typeString", 0)
    }
    Comment(content, ofType)
  }
}
