name := """scala-retrospective"""
organization := "it.doscienceto"

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayScala)

scalaVersion := "2.12.6"

resolvers += "jitpack" at "https://jitpack.io" 

libraryDependencies += guice
libraryDependencies += "org.scalatestplus.play" %% "scalatestplus-play" % "3.1.2" % Test
libraryDependencies += "com.github.kenglxn" % "qrgen" % "2.5.0"
// Adds additional packages into Twirl
//TwirlKeys.templateImports += "it.doscienceto.controllers._"

// Adds additional packages into conf/routes
// play.sbt.routes.RoutesKeys.routesImport += "it.doscienceto.binders._"
